import json
import time
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup

source_url = "https://blog.hubspot.com/marketing/networking-quotes"
print("==== Fetching datas from:", source_url)
s_html = urlopen(source_url).read()
soup_s = BeautifulSoup(s_html, "lxml")
print("Browsing:",soup_s.head.title.string)
elems_found = soup_s.find_all("div", {"class": "hsg-featured-snippet"})
quotes = []
for e in elems_found:
    for li in e.find_all('li'):
        quotes.append(li)
if len(quotes)>0:
    print("Found # quotes: ", len(quotes))
    with open('quotes.json','a') as f:
        j_quotes = []
        for q in quotes:
            j_quote = {}
            s = q.text.split("--")
            j_quote["quote"] = s[0]
            j_quote["author"] = s[1]
            j_quote["source"] = source_url
            j_quotes.append(j_quote)
        json.dump(j_quotes,f)
        print("Dumped # quotes: ", len(j_quotes))

source_url = "https://blog.hubspot.com/marketing/marketing-quotes"
print("==== Fetching datas from:", source_url)
s_html = urlopen(source_url).read()
soup_s = BeautifulSoup(s_html, "lxml")
print("Browsing:",soup_s.head.title.string)
elems_found = soup_s.find_all("li")
print("Found # elements: ", len(elems_found))
quotes = []
for e in elems_found:
    s = e.text.split(" — ")
    if len(s) == 2:
        quotes.append(e.text)
print("Found # quotes: ", len(quotes))
with open('quotes.json','a') as f:
    j_quotes = []
    for q in quotes:
        j_quote = {}
        s = q.split(" — ")
        j_quote["quote"] = s[0]
        j_quote["author"] = s[1]
        j_quote["source"] = source_url
        j_quotes.append(j_quote)
    json.dump(j_quotes,f)
    print("Dumped # quotes: ", len(j_quotes))
